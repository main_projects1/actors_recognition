# Actors Face Recognition

Проект по распознаванию лиц актеров с использованием OpenCV и deep learning.

## Установка
1. Установить библиотеку [dlib]([http://dlib.net/]), которая используется для построения face embeddings

#### Без поддержки GPU:
* с использованием pip:

`$ pip install dlib`
* собрать из исходников:

```
`$ git clone https://github.com/davisking/dlib.git`
`$ cd dlib`
`$ mkdir build`
`$ cd build`
`$ cmake .. -DUSE_AVX_INSTRUCTIONS=1`
`$ cmake --build .`
`$ cd ..`
`$ python setup.py install --yes USE_AVX_INSTRUCTIONS`
```
#### C поддержкой GPU:

Если у вас есть графический процессор, совместимый с CUDA, вы можете установить dlib с поддержкой графического процессора, что позволит увеличить скорость распознавания лиц.

```
$ git clone https://github.com/davisking/dlib.git
$ cd dlib
$ mkdir build
$ cd build
$ cmake .. -DDLIB_USE_CUDA=1 -DUSE_AVX_INSTRUCTIONS=1
$ cmake --build .
$ cd ..
$ python setup.py install --yes USE_AVX_INSTRUCTIONS --yes DLIB_USE_CUDA
```

2. Установить библиотеку [face_recognition]([https://github.com/ageitgey/face_recognition])

Модуль face_recognition устанавливается с помощью команды pip:

`$ pip install face_recognition`

3. Установить OpenCV, инструкция [в статье]([https://pyimagesearch.com/opencv-tutorials-resources-guides/])
4. Установить пакет imutils: `$ pip install imutils`

## Структура проекта распознавания лиц
Основные файлы проекта расположены в директории /src/vosk-api/python/
* **/dataset/** - директория, которая содержит изображения лиц шести актеров, организованные в подкаталоги в соответствии с их именами.
* **/examples/** - изображения лиц для тестирования, которых нет в наборе данных.
* **/output/** - директория для хранения обработанных видео с распознанными лицами.
* **/videos/** - видео для подачи на вход для распознавания.
* **encode_faces.py** - скрипт для построения кодировок (128-мерных векторов) для лиц.
* **recognize_faces_image.py** - скрипт для распознавания лиц по изображению (на основе полученных кодировок).
* **recognize_faces_video_file.py** - скрипт для распознавания лиц по видео-файлу;
* **encodings.pickle** - кодировки распознавания лиц, которые генерируются из набора данных с помощью encode_faces.py;

## Запуск
### Создание face embeddings:

`$ python encode_faces.py --dataset dataset --encodings encodings.pickle`

Описание параметров запуска доступно по команде:

`$ python encode_faces.py -h`

```console
usage: encode_faces.py [-h] -i DATASET -e ENCODINGS [-d DETECTION_METHOD]

options:
  -h, --help            show this help message and exit
  -i DATASET, --dataset DATASET
                        path to input directory of faces + images
  -e ENCODINGS, --encodings ENCODINGS
                        path to serialized db of facial encodings
  -d DETECTION_METHOD, --detection-method DETECTION_METHOD
                        face detection model to use: either `hog` or `cnn`
```

По окончанию результата выполнения скрипта создается файл encodings.pickle, который содержит 128-мерные векторы для каждого лица в наборе данных.
Полученные embeddings используются для распознавания актеров как на изображениях, так и по видео-файлам.

### Результаты распознавания
1. Распознавание лиц на изображениях:

На вход скрипта необходимо подать обученные на наборе данных кодировки и тестовое изображение.

`$ python recognize_faces_image.py --encodings encodings.pickle \
	--image examples/example_01.png`

Описание параметров запуска доступно по команде:

`$ python recognize_faces_image.py -h`

```console
usage: recognize_faces_image.py [-h] -e ENCODINGS -i IMAGE [-d DETECTION_METHOD]

options:
  -h, --help            show this help message and exit
  -e ENCODINGS, --encodings ENCODINGS
                        path to serialized db of facial encodings
  -i IMAGE, --image IMAGE
                        path to input image
  -d DETECTION_METHOD, --detection-method DETECTION_METHOD
                        face detection model to use: either `hog` or `cnn`
```

Пример полученного распознавания лиц по изображению:

![img.png](show/img.png)


2. Распознавание лиц по видео-файлам:

Скрипт принимает на вход видео-файл и на выходе генерирует выходной видео и txt файлы.

`$ python recognize_faces_video_file.py --encodings encodings.pickle \
	--input videos/lunch_scene.mp4 --output output/lunch_scene_output.avi \
	--display 0`

Описание параметров запуска доступно по команде:

`$ python recognize_faces_video_file.py -h`

```console
usage: recognize_faces_video_file.py [-h] -e ENCODINGS -i INPUT [-o OUTPUT] [-y DISPLAY] [-d DETECTION_METHOD] -t TXTFILE

options:
  -h, --help            show this help message and exit
  -e ENCODINGS, --encodings ENCODINGS
                        path to serialized db of facial encodings
  -i INPUT, --input INPUT
                        path to input video
  -o OUTPUT, --output OUTPUT
                        path to output video
  -y DISPLAY, --display DISPLAY
                        whether or not to display output frame to screen
  -d DETECTION_METHOD, --detection-method DETECTION_METHOD
                        face detection model to use: either `hog` or `cnn`
  -t TXTFILE, --txtfile TXTFILE
                        the name of output txt_file
```

Видео-файл с результатом распознавания лиц находится в директории output.

Структура txt-файла:

![img.png](show/txt_output.png)

 


